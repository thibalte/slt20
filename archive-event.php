<?php get_header(); ?>
<main class="main main-black" data-barba="container" data-barba-namespace="programm">
	<div class="main-filters">
		<div class="main-filters-group main-filters-group-1">
			<div class="main-filters-group-name"><?php str_e('Day'); ?>:</div>
			<div class="main-filters-group-items">
				<div class="main-filters-group-item" data-key="day" data-value="freitag"><?php str_e('Freitag'); ?></div>
				<div class="main-filters-group-item" data-key="day" data-value="samstag"><?php str_e('Samstag'); ?></div>
			</div>
		</div>
		<div class="main-filters-group main-filters-group-1">
			<div class="main-filters-group-name"><?php str_e('Room'); ?>:</div>
			<div class="main-filters-group-items">
				<div class="main-filters-group-item" data-key="room" data-value="saulenhalle"><?php str_e('Säulenhalle'); ?></div>
				<div class="main-filters-group-item" data-key="room" data-value="foyer"><?php str_e('Foyer'); ?></div>
				<div class="main-filters-group-item" data-key="room" data-value="seminarraum"><?php str_e('Seminarraum'); ?></div>
				<div class="main-filters-group-item" data-key="room" data-value="kneipe"><?php str_e('Kneipe'); ?></div>
			</div>
		</div>
		<div class="main-filters-group main-filters-group-1">
			<div class="main-filters-group-name"><?php str_e('Language'); ?>:</div>
			<div class="main-filters-group-items">
				<div class="main-filters-group-item" data-key="lang" data-value="de">de</div>
				<div class="main-filters-group-item" data-key="lang" data-value="fr">fr</div>
				<div class="main-filters-group-item" data-key="lang" data-value="it">it</div>
				<div class="main-filters-group-item" data-key="lang" data-value="en">en</div>
			</div>
		</div>
		<div class="main-filters-group main-filters-group-5">
			<div class="main-filters-group-name"><?php str_e('Type'); ?>:</div>
			<div class="main-filters-group-items">
				<div class="main-filters-group-item" data-key="type" data-value="podium"><?php str_e('Podium'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="gesprach"><?php str_e('Gespräch'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="diskussion"><?php str_e('Diskussion'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="ubersetzung"><?php str_e('Übersetzung'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="kinder"><?php str_e('Kinder- und Jugendliteratur'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="workshop"><?php str_e('Werkstatt'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="buchclub"><?php str_e('Buchclub'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="spielerisch"><?php str_e('Spielerisch'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="radio"><?php str_e('Radio'); ?></div>
			</div>
		</div>
	</div>
	<div class="main-day">
		<div class="main-day-title"><?php str_e('Samstag'); ?></div>
		<?php
			$freitag = get_posts( array(
				'posts_per_page'	=> -1,
				'post_type'			=> 'event',
				'meta_key'			=> 'time_start',
				'orderby'			=> 'meta_value_num',
				'order'				=> 'ASC',
				'meta_query'		=> array(
					array(
						'key'	 	=> 'day',
						'value'	  	=> 'samstag',
						'compare' 	=> '='
					)
				)
			));
		?>
		<div class="main-day-events">
			<?php if ($freitag): foreach($freitag as $event): ?>
				<div class="main-item main-day-event<?php
					// day
					$day = get_field('day', $event->ID);
					echo ' main-item-day-' .$day;

					// room
					$stage = get_field('stage', $event->ID);
					echo ' main-item-room-' .$stage['value'];

					// languages
					$languages = get_field('languages', $event->ID);
					if( $languages ):
						foreach( $languages as $language ):
							echo ' main-item-lang-' .$language;
						endforeach;
					endif;

					// type
					$type = get_field('type', $event->ID);
					echo ' main-item-type-' .$type['value'];

					// ID
					echo ' main-item-id-' .$event->ID;
				?>">
					<div class="main-day-inner">
						<div class="main-day-front">
							<div class="main-day-event-time">
								<div class="main-day-event-slot">
									<?php
										echo get_field('time_start', $event->ID);
										if (!get_field('hide_stop_time', $event->ID)) {
											echo '&ndash;'. get_field('time_stop', $event->ID);
										}
									?>
								</div>
								<div class="main-day-event-stage">
									<?php echo $stage['label']; ?>
								</div>
							</div>
							<div class="main-day-event-registration">
								<?php
									$register = get_field('registration', $event->ID);
									if ($register != 'no') {
										if ($register == 'required') str_e('Required');
										if ($register == 'few') {
											echo '<span class="main-day-event-registration-few">';
											str_e('Few');
											echo '</span>';
										}
										if ($register == 'full'){
											echo '<span class="main-day-event-registration-full">';
											str_e('Full');
											echo '</span>';
										}
									}
								?>
							</div>
							<div class="main-day-event-title">
								<?php echo get_the_title($event->ID); ?>
							</div>
							<div class="main-day-event-meta">
								<span class="bold">
									<?php
										// type
										str_e($type['label']);

										// languages
										if( $languages ):
											echo '(';
											$first = true;
											foreach( $languages as $language ):
												if (!$first) echo '/';
												echo $language;
												$first = false;
											endforeach;
											echo ')';
										endif;
									?>
								</span>
								<?php
									// writers
									$writers = get_field('writers', $event->ID);
									if( $writers ):
										foreach( $writers as $writer):
											echo '<a href="'.get_the_permalink($writer->ID).'">' .get_the_title($writer->ID). '</a> ';
										endforeach;
									endif;

									echo get_field('writers_without_profile', $event->ID);
									$extra = get_field('text_meta', $event->ID);
									if ($extra != '') echo '<br>'.$extra;
								?>
							</div>
							<div class="main-day-event-more">
								i
								<div class="main-day-event-more-black"></div>
							</div>
						</div>
						<div class="main-day-back">
							<div class="main-day-back-content">
								<h1><?php echo get_the_title($event->ID); ?></h1>
								<?php echo get_field('description', $event->ID); ?>
							</div>
							<?php
								if ($register != 'no' && $register != 'full') {
									echo '<div class="main-day-register" data-event-title="';
									echo get_the_title($event->ID);
									echo '" data-event-day="';
									echo get_field('day', $event->ID);
									echo '" data-event-room="';
									echo $stage['label'];
									echo '" data-event-time="';
									echo get_field('time_start', $event->ID);
									echo '"><span>';
									str_e('Register');
									echo '</span></div>';
								}
							?>
							<div class="main-day-close">✕</div>
						</div>
					</div>
				</div>
			<?php endforeach; endif; ?>
		</div>
		<div class="main-day" style="opacity: 0.5">
			<div class="main-day-title"><?php str_e('Freitag'); ?></div>
			<?php
				$freitag = get_posts( array(
					'posts_per_page'	=> -1,
					'post_type'			=> 'event',
					'meta_key'			=> 'time_start',
					'orderby'			=> 'meta_value_num',
					'order'				=> 'ASC',
					'meta_query'		=> array(
						array(
							'key'	 	=> 'day',
							'value'	  	=> 'freitag',
							'compare' 	=> '='
						)
					)
				));
			?>
			<div class="main-day-events">
				<?php if ($freitag): foreach($freitag as $event): ?>
					<div class="main-item main-day-event<?php
						// day
						$day = get_field('day', $event->ID);
						echo ' main-item-day-' .$day;

						// room
						$stage = get_field('stage', $event->ID);
						echo ' main-item-room-' .$stage['value'];

						// languages
						$languages = get_field('languages', $event->ID);
						if( $languages ):
							foreach( $languages as $language ):
								echo ' main-item-lang-' .$language;
							endforeach;
						endif;

						// type
						$type = get_field('type', $event->ID);
						echo ' main-item-type-' .$type['value'];

						// ID
						echo ' main-item-id-' .$event->ID;
					?>">
						<div class="main-day-inner">
							<div class="main-day-front">
								<div class="main-day-event-time">
									<div class="main-day-event-slot">
										<?php
											echo get_field('time_start', $event->ID);
											if (!get_field('hide_stop_time', $event->ID)) {
												echo '&ndash;'. get_field('time_stop', $event->ID);
											}
										?>
									</div>
									<div class="main-day-event-stage">
										<?php echo $stage['label']; ?>
									</div>
								</div>
								<div class="main-day-event-registration">
									<?php
										$register = get_field('registration', $event->ID);
										if ($register != 'no') {
											if ($register == 'required') {
												echo '<span>';
												str_e('Required');
												echo '</span>';
											} else if ($register == 'few') {
												echo '<span class="main-day-event-registration-few">';
												str_e('Few');
												echo '</span>';
											} else if ($register == 'full'){
												echo '<span class="main-day-event-registration-full">';
												str_e('Full');
												echo '</span>';
											}
										}
									?>
								</div>
								<div class="main-day-event-title">
									<?php echo get_the_title($event->ID); ?>
								</div>
								<div class="main-day-event-meta">
									<span class="bold">
										<?php
											// type
											str_e($type['label']);

											// languages
											if( $languages ):
												echo ' (';
												$first = true;
												foreach( $languages as $language ):
													if (!$first) echo '/';
													echo $language;
													$first = false;
												endforeach;
												echo ')';
											endif;
										?>
									</span>
									<?php
										// writers
										$writers = get_field('writers', $event->ID);
										if( $writers ):
											$count = 0;
											foreach( $writers as $writer):
												echo '<a href="'.get_the_permalink($writer->ID).'">' .get_the_title($writer->ID). '</a>';
												if ($count < count($writers)-1) echo ', ';
												$count++;
											endforeach;
										endif;

										$writers_without_profile = get_field('writers_without_profile', $event->ID);
										if ($writers_without_profile != '') echo ' ' .$writers_without_profile;

										$extra = get_field('text_meta', $event->ID);
										if ($extra != '') echo '<br>'.$extra;
									?>
								</div>
								<div class="main-day-event-more">
									i
									<div class="main-day-event-more-black"></div>
								</div>
							</div>
							<div class="main-day-back">
								<div class="main-day-back-content">
									<h1><?php echo get_the_title($event->ID); ?></h1>
									<?php echo get_field('description', $event->ID); ?>
								</div>
								<?php
									if ($register != 'no' && $register != 'full') {
										echo '<div class="main-day-register" data-event-title="';
										echo get_the_title($event->ID);
										echo '" data-event-day="';
										echo get_field('day', $event->ID);
										echo '" data-event-room="';
										echo $stage['label'];
										echo '" data-event-time="';
										echo get_field('time_start', $event->ID);
										echo '"><span>';
										str_e('Register');
										echo '</span></div>';
									}
								?>
								<div class="main-day-close">✕</div>
							</div>
						</div>
					</div>
				<?php endforeach; endif; ?>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>