<?php get_header(); ?>
<main class="main main-black" data-barba="container" data-barba-namespace="srf">
	<div class="main-filters">
		<div class="main-filters-group main-filters-group-6">
			<div class="main-filters-group-name"><?php str_e('Format'); ?>:</div>
			<div class="main-filters-group-items">
				<div class="main-filters-group-item" data-key="type" data-value="52-best-buecher"><?php str_e('52 Beste Bücher'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="literaturfenster-schweiz"><?php str_e('Literaturfenster Schweiz'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="schnabelweid"><?php str_e('Schnabelweid'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="videobeitraege"><?php str_e('Videobeiträge'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="waehrend-nach-solothurn"><?php str_e('Während/nach Solothurn'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="kulturplatz"><?php str_e('Kulturplatz'); ?></div>
			</div>
		</div>
	</div>
	<div class="main-srf">
		<div class="main-srf-title"><?php str_e('Presse Bistro'); ?></div>
		<?php
			$srf = get_posts( array(
				'posts_per_page'	=> -1,
				'post_type'			=> 'srf',
				'orderby'			=> 'title',
				'order'				=> 'ASC',
				'post_status' 		=> 'any'
			));
		?>
		<div class="main-srf-items">
			<?php if ($srf): foreach($srf as $event): ?>
				<div class="main-item main-srf-item<?php
					// type
					$type = get_field('type', $event->ID);
					echo ' main-item-type-' .$type['value'];
				?>">
					<div class="main-srf-inner">
						<div class="main-srf-front">
							<div class="main-srf-title">
								<?php echo get_the_title($event->ID); ?>
							</div>
							<div class="main-srf-more">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/srgssr-black.svg">
							</div>
						</div>
						<div class="main-srf-back">
							<div class="main-srf-back-content">
								<?php echo get_field('subtitle', $event->ID); ?>
								<img src="<?php echo get_field('cover', $event->ID)['sizes']['large']; ?>">
								<div class="main-srf-link">
									<a href="<?php echo get_field('link', $event->ID); ?>" target="_blank">
										<?php str_e('Sendung'); ?>
									</a>
								</div>
							</div>
							<div class="main-srf-close">✕</div>
						</div>
					</div>
				</div>
			<?php endforeach; endif; ?>
		</div>
	</div>
</main>
<?php get_footer(); ?>