<?php /* Template Name: Resonanzraum */ ?>

<?php get_header(); ?>
<main class="main main-black" data-barba="container">
	<div class="main-resonanzraum">
		<?php if (have_posts()): while (have_posts()): the_post(); ?>
			<div class="main-resonanzraum-content">
				<h1><?php echo get_the_title(); ?></h1>
				<div class="main-resonanzraum-content-buffer">
					<?php the_content(); ?>
				</div>
				<a href="<?php echo get_the_permalink(pll_get_post(get_page_by_title('Resonanzraum_SLT')->ID)); ?>" class="main-resonanzraum-link">
					@RESONANZRAUM_SLT
				</a>
				<a href="<?php echo get_the_permalink(pll_get_post(get_page_by_title('Bolo Klub')->ID)); ?>" class="main-resonanzraum-link">
					<?php str_e('BOLO KLUB'); ?>
				</a>
			</div>
		<?php endwhile; endif; ?>
	</div>
</main>
<?php get_footer(); ?>