<?php /* Template Name: Resonanzraum SLT*/ ?>

<?php get_header(); ?>
<main class="main main-black" data-barba="container">
	<div class="main-resonanzraum-slt">
		<?php if (have_posts()): while (have_posts()): the_post(); ?>
			<div class="main-resonanzraum-slt-content">
				<?php the_content(); ?>
			</div>
		<?php endwhile; endif; ?>
	</div>
</main>
<?php get_footer(); ?>