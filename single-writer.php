<?php get_header(); ?>
<main class="main main-black" data-barba="container" data-barba-namespace="gaeste">
	<div class="main-single-author">
		<?php if (have_posts()): while (have_posts()): the_post(); ?>
			<h1><?php the_title(); ?></h1>
			<div class="main-single-author-portrait">
				<?php $portrait = get_field('portrait'); ?>
				<img src="<?php echo $portrait['sizes']['large']; ?>">
				<p><?php echo $portrait['caption']; ?></p>
			</div>
			<div class="main-single-author-bio">
				<?php echo get_field('biography'); ?>
			</div>

			<?php

				// BOOKS

				$q = array(
					'posts_per_page'	=> -1,
					'post_type'			=> 'book',
					'orderby'			=> 'title',
					'order'				=> 'ASC',
					'meta_query' => array(
						'relation' => 'OR',
						array(
							'key' => 'writer',
							'value' => get_the_ID(),
							'compare' => '='
						),
						array(
							'key' => 'translator',
							'value' => get_the_ID(),
							'compare' => '='
						)
					)
				);

				$books = get_posts($q);

				if ($books): foreach($books as $book): ?>
				<div id="<?php echo $book->ID; ?>" class="main-single-writer-book <?php echo 'main-single-writer-book-' .$book->ID; ?>">
					<div class="main-single-writer-book-wrap">
						<div class="main-single-writer-book-row">
							<div class="main-single-writer-book-col main-single-writer-book-col-2">
								<h1><?php echo get_the_title($book->ID); ?></h1>
								<p class="mono"><?php echo get_field('details', $book->ID); ?></p>
								<div class="main-book-buttons">
									<?php $buy = get_field('link', $book->ID); ?>
									<?php if ($buy != ''): ?>
									<span class="main-book-buy">
										<a href="<?php echo $buy; ?>" target="_blank"><?php str_e('Buy'); ?></a>
									</span>
									<?php endif; ?>
								</div>
							</div>
							<div class="main-single-writer-book-col main-single-writer-book-col-1">
								<img src="<?php echo get_field('cover', $book->ID)['sizes']['large']; ?>">
							</div>
						</div>
						<?php
							$quote = get_field('quote', $book->ID);
							$description = get_field('description', $book->ID);
						?>
						<?php if ($quote != ''): ?>
						<div class="main-single-writer-book-row">
							<div class="main-single-writer-book-col main-single-writer-book-col-1">
							</div>
							<div class="main-single-writer-book-col main-single-writer-book-col-2 alpina">
								<?php echo get_field('quote', $book->ID); ?>
							</div>
						</div>
						<?php endif; if ($description != ''): ?>
						<div class="main-single-writer-book-row">
							<?php echo get_field('description', $book->ID); ?>
						</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach; endif; ?>
			<?php

				// LOGBUCH

				$q = array(
					'posts_per_page'	=> -1,
					'post_type'			=> 'logbook',
					'orderby'			=> 'title',
					'order'				=> 'ASC',
					'meta_query' => array(
						array(
							'key' => 'writers',
							'value' => '"' .get_the_ID(). '"',
							'compare' => 'LIKE'
						)
					)
				);

				$logbooks = get_posts($q);

				if ($logbooks): foreach($logbooks as $logbook): ?>
				<div class="main-single-writer-logbook">
					<div class="main-single-writer-logbook-date">
						<?php str_e('Logbuch'); ?> <?php echo get_the_date('d.m.Y', $logbook->ID); ?>
					</div>
					<h1 class="main-single-writer-logbook-title-<?php echo get_field('logbuch_type', $logbook->ID); ?>"><a href="<?php echo get_the_permalink($logbook->ID); ?>"><?php echo get_the_title($logbook->ID); ?></a></h1>
				</div>
			<?php endforeach; endif; ?>
			<?php

				$q = array(
					'posts_per_page'	=> -1,
					'post_type'			=> 'event',
					'orderby'			=> 'date',
					'order'				=> 'ASC',
					'meta_query' => array(
						array(
							'key' => 'writers',
							'value' => '"' . get_the_ID() . '"',
							'compare' => 'LIKE'
						)
					)
				);

				$live = get_posts($q);

				if ($live): ?>
				<div class="main-single-writer-live">
					<h1>LIVE</h1>
					<div class="main-single-writer-events">
						<?php foreach($live as $e): ?>
							<div class="main-single-writer-event">
								<div class="main-single-writer-event-col main-single-writer-event-col-2">
									<?php
										str_e(get_field('day', $e->ID));
										echo ' ' .get_field('time_start', $e->ID);
										if (get_field('with_stop', $e->ID)) echo '&ndash;' .get_field('time_stop', $e->ID);
									?>
								</div>
								<div class="main-single-writer-event-col main-single-writer-event-col-2">
									<?php
										echo get_the_title($e->ID);
									?>
								</div>
								<div class="main-single-writer-event-col">
									<?php
										echo get_field('stage', $e->ID)['label'];
									?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<?php endif; ?>
		</div>
		<?php endwhile; endif; ?>
	</div>
</main>
<?php get_footer(); ?>