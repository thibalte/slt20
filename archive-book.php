<?php get_header(); ?>
<main class="main main-black" data-barba="container">
	<div class="main-books-header">
		<h1><?php str_e('Books Header'); ?></h1>
	</div>
	<div class="main-books">
		<?php
			$books = get_posts( array(
				'posts_per_page'	=> -1,
				'post_type'			=> 'book',
				'orderby'			=> 'title',
				'order'				=> 'ASC'
			));
		?>
		<?php if ($books): foreach($books as $book): ?>
			<div class="main-book">
				<div class="main-book-cover">
					<?php

						$writer = false;

						if (get_field('translator_has_profile', $book->ID) == 1) $writer = get_field('translator', $book->ID);
						else if (get_field('writer_has_profile', $book->ID) == 1) $writer = get_field('writer', $book->ID);

						if ($writer):
					?>
					<a href="<?php echo get_the_permalink($writer); echo '#'.$book->ID; ?>">
					<?php endif; ?>
						<img src="<?php echo get_field('cover', $book->ID)['sizes']['large']; ?>">
					<?php if ($writer): ?>
					</a>
					<?php endif; ?>
				</div>
				<div class="main-book-infos">
					<?php echo get_the_title($book->ID); ?><br>
					<?php
						// writers
						if (get_field('writer_has_profile', $book->ID) == 1) echo get_the_title(get_field('writer', $book->ID));
						else echo get_field('text_writer', $book->ID);
					?>
				</div>

				<div class="main-book-buttons">
					<?php $buy = get_field('link', $book->ID); ?>
					<?php if ($buy != ''): ?>
					<span class="main-book-buy">
						<a href="<?php echo $buy; ?>" target="_blank"><?php str_e('Buy'); ?></a>
					</span>
					<?php endif; ?>
				</div>
			</div>
		<?php endforeach; endif; ?>
	</div>
</main>
<?php get_footer(); ?>