		<div class="registration" style="display: none;">
			<p class="registration-title"></p>
			<?php
				$l = pll_current_language();
				switch($l){
					case 'de':
						echo do_shortcode('[contact-form-7 id="1721" title="Registration DE"]');
						break;
					case 'fr':
						echo do_shortcode('[contact-form-7 id="1754" title="Registration FR"]');
						break;
					case 'it':
						echo do_shortcode('[contact-form-7 id="1757" title="Registration IT"]');
						break;
				}
			?>
			<div class="registration-success" style="display: none;"><p><?php str_e('Success'); ?></p></div>
			<div class="registration-failure" style="display: none;"><p><?php str_e('Failure'); ?></p></div>
			<div class="registration-sending" style="display: none;"><p><?php str_e('Sending...'); ?></p></div>
			<div class="registration-close">✕</div>
		</div>
		<footer class="footer">
			<div class="footer-col footer-col-1">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/slt-logo-footer.svg">
			</div>
			<div class="footer-col footer-col-1 footer-buffer">
				<p>
					<a href="<?php echo get_the_permalink(pll_get_post(get_page_by_title('Info')->ID)); ?>">Info</a><br>
					<a href="<?php echo get_the_permalink(pll_get_post(get_page_by_title('Impressum und Haftungsausschluss')->ID)); ?>">Impressum</a><br>
					<a href="https://www.literatur.ch/de/service/medien-9.html" target="_blank"><?php str_e('Medien / Médias / Media'); ?></a>
				</p>
				<p>
					<a href="https://www.instagram.com/solothurnerliteraturtage/" target="_blank">Instagram</a><br>
					<a href="https://www.facebook.com/SolothurnerLiteraturtage/" target="_blank">Facebook</a><br>
					<a href="https://twitter.com/soliteraturtage" target="_blank">Twitter</a><br>
					<a href="https://us10.list-manage.com/subscribe?u=aab18e49303547cbbc7265bf2&id=fb4e7623b6" target="_blank">Newsletter</a>
				</p>
			</div>
			<div class="footer-col footer-col-1 footer-buffer">
				<p>
					Solothurner Literaturtage<br>
					Dornacherplatz 15a<br>
					Postfach<br>
					4502 Solothurn
				</p>
				<p>
					<a href="http://www.literatur.ch" target="_blank">www.literatur.ch</a><br>
					<a href="mailto:info@literatur.ch" target="_blank">info@literatur.ch</a><br>
					+41 32 622 44 11
				</p>
			</div>
			<div class="footer-col footer-col-2 footer-buffer footer-sponsors">
				<div class="footer-sponsor">
					<a href="https://so.ch/" target="_blank">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ktsolothurn.svg">
					</a>
				</div><div class="footer-sponsor">
					<a href="https://www.stadt-solothurn.ch/" target="_blank">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/stadthsolothurn.svg">
					</a>
				</div><div class="footer-sponsor">
					<a href="https://prohelvetia.ch/" target="_blank">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/prohelvetia.svg">
					</a>
				</div><div class="footer-sponsor">
					<a href="https://www.srf.ch/" target="_blank">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/srgssr.svg">
					</a>
				</div><div class="footer-sponsor">
					<a href="https://www.buchhaus.ch/" target="_blank">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/luethy.svg">
					</a>
				</div><div class="footer-sponsor">
					<a href="https://www.selection-schwander.ch/" target="_blank">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/selectionschwander.svg">
					</a>
				</div>
			</div>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>