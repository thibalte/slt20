<?php /* Template Name: Bolo Klub*/ ?>

<?php get_header(); ?>
<main class="main main-black" data-barba="container" data-barba-namespace="bolo">
	<div class="main-bolo-klub">
		<?php if (have_posts()): while (have_posts()): the_post(); ?>
			<div class="main-bolo-klub-content">
				<h1><?php echo get_the_title(); ?></h1>
				<?php if (have_rows('bolo_items')): ?>
					<div class="main-bolo-klub-gallery">
						<?php while(have_rows('bolo_items')): the_row() ?>
							<div class="main-bolo-klub-slide"
								data-name="<?php echo get_sub_field('name'); ?>"
								data-email="<?php echo get_sub_field('email'); ?>"
								data-website="<?php echo get_sub_field('website'); ?>"
								data-instagram="<?php echo get_sub_field('instagram'); ?>"
								data-technik="<?php echo get_sub_field('technik'); ?>"
								data-format="<?php echo get_sub_field('format'); ?>"
								data-preis="<?php echo get_sub_field('preis'); ?>"
								data-sold="<?php echo get_sub_field('sold'); ?>">
								<img src="<?php echo get_sub_field('image')['sizes']['large']; ?>">
							</div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
				<p>
					© <a href="" class="main-bolo-klub-name"></a> - <a href="" target="_blank" class="main-bolo-klub-website"></a> <a href="" target="_blank" class="main-bolo-klub-instagram"></a><br>
					<span class="main-bolo-klub-technik"></span>, <span class="main-bolo-klub-format"></span><br>
					CHF <span class="main-bolo-klub-price"></span>.-<br>
					<a href="" class="main-bolo-klub-buy" style="display: none;"><?php str_e('Kaufen'); ?></a>
					<span class="main-bolo-klub-sold" style="display: none;"><?php str_e('Sold'); ?></span>
				</p>
			</div>
		<?php endwhile; endif; ?>
	</div>
</main>
<?php get_footer(); ?>