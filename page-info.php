<?php /* Template Name: Info */ ?>

<?php get_header(); ?>
<main class="main" data-barba="container" data-barba-namespace="info">
	<div class="main-info">
		<?php if (have_posts()): while (have_posts()): the_post(); ?>
			<div class="main-info-content">
				<?php the_content(); ?>
				<h2><?php echo get_field('gallery_1_title'); ?></h2>
				<?php
				$sponsors = get_field('gallery_1');
				if($sponsors): ?>
					<div class="main-info-gallery">
						<?php foreach($sponsors as $sponsor): ?>
							<div class="main-info-gallery-item">
								<img src="<?php echo $sponsor['sizes']['medium']; ?>">
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
				<h2><?php echo get_field('gallery_2_title'); ?></h2>
				<?php
				$sponsors = get_field('gallery_2');
				if($sponsors): ?>
					<div class="main-info-gallery">
						<?php foreach($sponsors as $sponsor): ?>
							<div class="main-info-gallery-item">
								<img src="<?php echo $sponsor['sizes']['medium']; ?>">
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
				<h2><?php echo get_field('gallery_3_title'); ?></h2>
				<?php
				$sponsors = get_field('gallery_3');
				if($sponsors): ?>
					<div class="main-info-gallery">
						<?php foreach($sponsors as $sponsor): ?>
							<div class="main-info-gallery-item">
								<img src="<?php echo $sponsor['sizes']['medium']; ?>">
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endwhile; endif; ?>
	</div>
</main>
<?php get_footer(); ?>