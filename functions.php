<?php

	function slt_menu(){
		register_nav_menu('main-menu',__( 'Main Menu' ));
	}

	function scripts() {
		wp_deregister_script( 'jquery' );

		wp_dequeue_style( 'wp-block-library' );
		wp_dequeue_style( 'wp-block-library-theme' );

		wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', array(), '1');
		wp_enqueue_script( 'marquee', '//cdn.jsdelivr.net/npm/jquery.marquee@1.5.0/jquery.marquee.min.js', array(), '1');
		wp_enqueue_script( 'barba', '//unpkg.com/@barba/core', array(), '2' );
		wp_enqueue_script( 'flickity-js', '//cdnjs.cloudflare.com/ajax/libs/flickity/2.2.1/flickity.pkgd.min.js', array(), '1' );
		wp_enqueue_script( 'app', get_stylesheet_directory_uri(). '/assets/js/app.min.js', array('jquery'), '20' );
	}

	function styles() {
		wp_enqueue_style( 'normalize', get_stylesheet_directory_uri(). '/assets/vendors/normalize.css', array() );
		wp_enqueue_style( 'flickity-css', '//cdnjs.cloudflare.com/ajax/libs/flickity/2.2.1/flickity.min.css', array() );
		wp_enqueue_style( 'css', get_stylesheet_directory_uri(). '/style.css', array(), '28' );
	}

	// add_filter( 'wpcf7_load_js', '__return_false' );
	add_filter( 'wpcf7_load_css', '__return_false' );

	add_action('wp_enqueue_scripts','scripts');
	add_action( 'wp_enqueue_scripts', 'styles' );
	add_action( 'init', 'slt_menu' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	add_filter('show_admin_bar', '__return_false');

  // REGISTER STRING TRANSLATIONS
	if (function_exists('pll_register_string')) {
		// pll_register_string('thursday', 'Thursday');
		pll_register_string('required', 'Required');
		pll_register_string('few', 'Few');
		pll_register_string('full', 'Full');
		pll_register_string('register', 'Register');
		pll_register_string('buy', 'Buy');
		pll_register_string('logbook', 'Logbuch');
		pll_register_string('freitag', 'Freitag');
		pll_register_string('samstag', 'Samstag');
		pll_register_string('freitag', 'freitag');
		pll_register_string('samstag', 'samstag');

		// program categories
		pll_register_string('podium', 'Podium');
		pll_register_string('gesprach', 'Gespräch');
		pll_register_string('diskussion', 'Diskussion');
		pll_register_string('ubersetzung', 'Übersetzung');
		pll_register_string('kinder', 'Kinder- und Jugendliteratur');
		pll_register_string('workshop', 'Werkstatt');
		pll_register_string('buchclub', 'Buchclub');
		pll_register_string('spielerisch', 'Spielerisch');
		pll_register_string('radio', 'Radio');

		// Logbuch categories
		pll_register_string('prosa', 'Prosa');
		pll_register_string('lyrik', 'Lyrik');
		pll_register_string('spoken', 'Spoken Word');
		pll_register_string('ubersetzung', 'Übersetzung');
		pll_register_string('kinder', 'Kinder- und Jugendliteratur');
		pll_register_string('int-gaste', 'Int. Gäste');
		pll_register_string('illustration', 'Illustration');
		pll_register_string('dialog', 'Im Dialog');
		pll_register_string('xxx', 'XXX');

		// extra
		pll_register_string('solothurner', '42. Solothurner');
		pll_register_string('media', 'Medien / Médias / Media');
		pll_register_string('day', 'Day');
		pll_register_string('room', 'Room');
		pll_register_string('language', 'Language');
		pll_register_string('type', 'Type');
		pll_register_string('success', 'Success');
		pll_register_string('failure', 'Failure');
		pll_register_string('book-header', 'Books Header');
		pll_register_string('bolo-klub', 'Bolo Klub');
		pll_register_string('format', 'Format');

		// SRF
		pll_register_string('52-best-buecher', '52 Beste Bücher');
		pll_register_string('literaturfenster-schweiz', 'Literaturfenster Schweiz');
		pll_register_string('schnabelweid', 'Schnabelweid');
		pll_register_string('videobeitraege', 'Videobeiträge');
		pll_register_string('waehrend-nach-solothurn', 'Während/nach Solothurn');
		pll_register_string('presse-bistro', 'Presse Bistro');
		pll_register_string('sendung', 'Sendung');
		pll_register_string('kulturplatz', 'Kulturplatz');
		pll_register_string('comments', 'Comments');

		pll_register_string('wine1', 'wine-1');
		pll_register_string('wine2', 'wine-2');
	}

	function str_e($str){
		if (function_exists('pll_e')){
			pll_e($str);
		} else {
			echo $str;
		}
	}

	function get_live_events() {

		if ( false === ( $result = get_transient('live'))) {
			$rooms = array('saulenhalle' => 'SÄULENHALLE', 'seminarraum' => 'SEMINARRAUM', 'kneipe' => 'KNEIPE', 'foyer' => 'FOYER');
			$live = [];

			foreach($rooms as $room => $roomLabel){
				$events = get_current($room);
				if ($events) {
					$live[$room] = array(
						'status' => 'now',
						'title' => $events[0]->post_title,
						'url' => get_the_permalink($events[0]->ID),
						'id' => $events[0]->ID
					);
				} else {
					$events = get_next($room);
					if ($events) {
						$live[$room] = array(
							'status' => 'next',
							'title' => $events[0]->post_title,
							'url' => get_the_permalink($events[0]->ID),
							'time' => get_field('time_start', $events[0]->ID),
							'id' => $events[0]->ID
						);
					} else $live[$room] = array('status'=>'end', 'event'=>null);
				}

				$args = array(
					'name'        => $room,
					'post_type'   => 'stream',
					'numberposts' => 1
				);
				$stream = get_posts($args);
				if ($stream) {
					$live[$room]['chat'] = get_field('enable_chat', $stream[0]->ID);
				}
			}

			$today = strtolower(date('l'));
			if ($today == 'friday') $today = 'freitag';
			if ($today == 'saturday') $today = 'samstag';

			$gmt = date('H:i:s');
			$now = strtotime($gmt) + 7200;
			$now = date('H:i:s', $now);

			$live['ts'] = array(
				'day' => $today,
				'time' => $now
			);

			$result = json_encode($live);

			set_transient( 'live', $result, 60 );
		}

		return $result;
	}

	function get_current($room){

		$today = strtolower(date('l'));
		if ($today == 'friday') $today = 'freitag';
		if ($today == 'saturday') $today = 'samstag';

		$gmt = date('H:i:s');
		$now = strtotime($gmt) + 7200;
		$now = date('H:i:s', $now);

		$events = get_posts( array(
			'posts_per_page'	=> 1,
			'post_type'			=> 'event',
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'day',
					'value' => $today, // $today
					'compare' => '=='
				),
				array(
					'key' => 'stage',
					'value' => $room, // $today
					'compare' => '=='
				),
				array(
					'key'           => 'time_start',
					'compare'       => '<=',
					'value'         => $now,
					'type'          => 'TIME'
				),
				array(
					'key'           => 'time_stop',
					'compare'       => '>',
					'value'         => $now,
					'type'          => 'TIME'
				)
			)
		));

		return $events;
	}

	function get_next($room){

		$today = strtolower(date('l'));
		if ($today == 'friday') $today = 'freitag';
		if ($today == 'saturday') $today = 'samstag';

		$gmt = date('H:i:s');
		$now = strtotime($gmt) + 7200;
		$now = date('H:i:s', $now);

		$events = get_posts( array(
			'posts_per_page'	=> 1,
			'post_type'			=> 'event',
			'orderby'			=> 'meta_value',
			'order'				=> 'ASC',
			'meta_type'			=> 'CHAR',
			'meta_key'			=> 'time_start',
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'day',
					'value' => $today, // $today
					'compare' => '=='
				),
				array(
					'key' => 'stage',
					'value' => $room, // $today
					'compare' => '=='
				),
				array(
					'key'           => 'time_start',
					'compare'       => '>=',
					'value'         => $now,
					'type'          => 'TIME'
				)
			)
		));

		return $events;
	}

	add_action( 'rest_api_init', function () {
		register_rest_route( 'slt/', '/live', array(
			'methods' => 'GET',
			'callback' => 'get_live_events',
		));
	})

	/*----- disable comments -----*/

	// Removes from admin menu
	/*
	add_action( 'admin_menu', 'my_remove_admin_menus' );
	function my_remove_admin_menus() {
		remove_menu_page( 'edit-comments.php' );
	}

	// Removes from post and pages
	add_action('init', 'remove_comment_support', 100);
	function remove_comment_support() {
		remove_post_type_support( 'post', 'comments' );
		remove_post_type_support( 'page', 'comments' );
	}

	// Removes from admin bar
	function mytheme_admin_bar_render() {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('comments');
	}
	add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );
	*/

?>