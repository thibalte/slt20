<?php get_header(); ?>
<main class="main main-black" data-barba="container" data-barba-namespace="aareufer">
	<div class="main-live-rooms" style="display: none;">
		<div class="main-live-side">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/LIVE.svg">
		</div>
		<div class="main-live-blocks">
			<div class="main-live-block main-live-saulenhalle" data-stream="saulenhalle">
				<div class="main-live-scroller" style="display: none;">LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE </div>
				<h1><a href="/stream/saulenhalle">SÄULENHALLE</a></h1>
				<div class="main-live-block-time" style="display: none;"></div>
				<div class="main-live-block-title" style="display: none;"></div>
			</div>
			<div class="main-live-block main-live-foyer" data-stream="foyer">
				<div class="main-live-scroller" style="display: none;">LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE </div>
				<h1><a href="/stream/foyer">FOYER</a></h1>
				<div class="main-live-block-time" style="display: none;"></div>
				<div class="main-live-block-title" style="display: none;"></div>
			</div>
			<div class="main-live-block main-live-seminarraum" data-stream="seminarraum">
				<div class="main-live-scroller" style="display: none;">LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE </div>
				<h1><a href="/stream/seminarraum">SEMINARRAUM</a></h1>
				<div class="main-live-block-time" style="display: none;"></div>
				<div class="main-live-block-title" style="display: none;"></div>
			</div>
			<div class="main-live-block main-live-kneipe" data-stream="kneipe">
				<div class="main-live-scroller" style="display: none;">LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE LIVE </div>
				<h1><a href="/stream/kneipe">KNEIPE</a></h1>
				<div class="main-live-block-time" style="display: none;"></div>
				<div class="main-live-block-title" style="display: none;"></div>
			</div>
		</div>
		<div class="main-live-side">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/LIVE.svg">
		</div>
	</div>
	<div class="main-header">
		<div class="main-title">
			<?php str_e('42. Solothurner'); ?>
		</div>
		<div class="main-links">
			<div class="main-links-default">
				<?php
					$homepage = get_post(pll_get_post(get_page_by_title('Homepage')->ID));
					echo apply_filters('the_content', $homepage->post_content);
				?>
			</div>
		</div>
		<div class="main-now">
			<div class="main-now-col">

				<?php str_e('Solothurn'); ?>:<br>

				<?php
					// OWM API key: 033c281e4d45c3ae1f88411184d1bb83
					// Solothurn ID: 2658564

					$ch = curl_init("http://api.openweathermap.org/data/2.5/weather?id=2658564&appid=033c281e4d45c3ae1f88411184d1bb83");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					$data = curl_exec($ch);
					curl_close($ch);

					$weather = json_decode($data);

					// echo $weather->weather[0]->main .'<br>';
					echo round($weather->main->temp - 273.15) . '°C<br>';
					echo round($weather->wind->speed) . 'km/h<br>';

					// echo var_dump($weather);
					// echo var_dump($weather);
					// echo $weather['weather'];

					/*
					if ($weather->'cod' == 200) {
						// echo $weather->''
					}
					*/

					// echo $weather;
				?>

				<p class="main-now-social">
					<a href="https://www.instagram.com/solothurnerliteraturtage/" target="_blank">Instagram</a><br>
					<a href="https://www.facebook.com/SolothurnerLiteraturtage/" target="_blank">Facebook</a><br>
					<a href="https://twitter.com/soliteraturtage" target="_blank">Twitter</a><br>
					<a href="https://us10.list-manage.com/subscribe?u=aab18e49303547cbbc7265bf2&id=fb4e7623b6" target="_blank">Newsletter</a>
				</p>
			</div>
			<div class="main-now-col">
				<canvas class="clock" width="150" height="150"></canvas>
			</div>
		</div>
	</div>
	<div class="main-home-news">
		<div class="main-home-news-row">
			<?php
				$q = array(
					'posts_per_page' => 3,
					'post_type' => 'news',
					'orderby' => 'date',
					'order' => 'DESC'
				);

				$news = get_posts($q);

				if ($news): foreach($news as $n):
			?>
			<div class="main-home-news-col">
				<span><?php echo get_the_date('d.m.Y', $n->ID); ?></span>
				<h2><?php echo $n->post_content; ?></h2>
			</div>
		<?php endforeach; endif; ?>

		</div>
		<div class="main-home-news-more" style="display: none;">
			<a href="/news">+ News</a>
		</div>
	</div>
	<div class="main-wines">
		<div class="main-wine">
			<a href="/de/was-es-an-den-onlineliteraturtagen-gibt-und-was-es-nicht-gibt/"><?php str_e('wine-1'); ?></a>
		</div>
		<div class="main-wine">
			<a href="/de/was-es-an-den-onlineliteraturtagen-gibt-und-was-es-nicht-gibt/"><?php str_e('wine-2'); ?></a>
		</div>
	</div>
	<div class="main-logbooks main-logbooks-aareufer">
		<?php

			/*
			$today = date('Ymd');

			$redaktionell = get_posts( array(
				'posts_per_page'	=> -1,
				'post_type'			=> 'logbook',
				'orderby'			=> 'rand',
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => 'aareufer_date',
						'value' => $today,
						'compare' => '=='
					),
					array(
						'key' => 'logbuch_type',
						'value' => 'redaktionell',
						'compare' => '=='
					)
				)
			));

			$literatur = get_posts( array(
				'posts_per_page'	=> -1,
				'post_type'			=> 'logbook',
				'orderby'			=> 'rand',
				'date_query' => array(
					'after' => 'today',
					'inclusive' => true,
				),
				'meta_key' => 'logbuch_type',
				'meta_value' => 'literatur'
			));

			$logbooks = array_merge($redaktionell, $literatur);
			shuffle($logbooks);
			*/

			$logbooks = get_posts( array(
				'posts_per_page'	=> 10,
				'post_type'			=> 'logbook',
				'orderby'			=> 'rand'
			));

		?>
		<?php if ($logbooks): foreach($logbooks as $logbook): ?>
			<div class="main-item main-logbook<?php
				// languages
				$languages = get_field('languages', $logbook->ID);
				if( $languages ):
					foreach( $languages as $language ):
						echo ' main-item-lang-';
						echo $language;
					endforeach;
				endif;
			?> main-item-type-<?php echo get_field('type', $logbook->ID)['value']; ?>">
				<div class="main-logbook-cover">
					<?php
						$cover = false;
						if (get_field('with_cover', $logbook->ID)) $cover = get_field('cover', $logbook->ID);
						if ($cover) {
							echo '<img src="' .$cover['sizes']['large']. '">';
						}
					?>
				</div>
				<div class="main-logbook-date">
					<!-- <?php str_e('Logbuch'); ?> <?php echo get_the_date('d.m.Y', $logbook->ID); ?> !-->
				</div>
				<div class="main-logbook-title main-logbook-title-<?php echo get_field('logbuch_type', $logbook->ID); ?>">
					<a href="<?php echo get_the_permalink($logbook->ID); ?>">
						<?php echo get_the_title($logbook->ID); ?>
					</a>
				</div>
				<?php $subtitle = get_field('subtitle', $logbook->ID); ?>
				<?php if ($subtitle): ?>
				<div class="main-logbook-meta">
					<span class="italic"><?php echo $subtitle; ?></span>
				</div>
				<?php endif; ?>
			</div>
		<?php endforeach; endif; ?>
	</div>
	<div class="main-ads">
		<?php
			$ads = get_posts( array(
				'posts_per_page'	=> 2,
				'post_type'			=> 'ad',
				'orderby'			=> 'rand'
			));
		?>
		<?php if ($ads): foreach($ads as $ad): ?>
		<div class="main-ad main-ad-<?php echo get_field('size', $ad->ID); ?>">
			<a href="<?php echo get_field('link', $ad->ID); ?>" target="_blank">
				<img src="<?php echo get_field('image', $ad->ID)['sizes']['large']; ?>">
			</a>
		</div>
		<?php endforeach; endif; ?>
	</div>
</main>
<?php get_footer(); ?>