<?php get_header(); ?>
<main class="main main-black" data-barba="container">
	<div class="main-authors">
		<?php
			$authors = get_posts( array(
				'posts_per_page'	=> -1,
				'post_type'			=> 'writer',
				'orderby'			=> 'meta_key',
				'order'				=> 'ASC',
				'meta_key'			=> 'lastname',
				'post_status'		=> 'publish'
			));
		?>
		<?php if ($authors): foreach($authors as $author): ?>
			<div class="main-author">
				<div class="main-author-portrait">
					<a href="<?php echo get_the_permalink($author->ID); ?>">
						<img src="<?php echo get_field('portrait', $author->ID)['sizes']['medium']; ?>">
					</a>
				</div>
				<div class="main-author-name">
					<?php echo get_the_title($author->ID); ?>
				</div>
			</div>
		<?php endforeach; endif; ?>
	</div>
</main>
<?php get_footer(); ?>