/* jshint asi:true */

console.log('Solothurner Literaturtage 2020')
console.log('###################')
console.log('design: Patrick Savolainen, https://www.affoltersavolainen.ch/en/	')
console.log('code: Thibault Brevet, http://www.site-specific.ch')

jQuery(document).ready(function($) {

	var clock, mobile = false
	var refresh = true

	barba.init({
		timeout: 5000,
		transitions: [{
			leave: function(data) {
				var done = this.async()
				$('html').addClass('loading')

				if (mobile)  $('.nav').fadeOut()

				$('.footer').animate({ opacity: 0 }, 250)

				$(data.current.container).animate({ opacity: 0 }, 250, function () {
					$(data.current.container).hide()
					done()
				})
			},
			enter: function(data) {
				var done = this.async()

				$('html').removeClass('loading')
				$('html, body').scrollTop(0)

				var $nav = $(data.next.html).find('.nav-lang')
				if ($nav){
					$('#lang-fr').attr('href', $nav.find('#lang-fr').attr('href'))
					$('#lang-de').attr('href', $nav.find('#lang-de').attr('href'))
					$('#lang-it').attr('href', $nav.find('#lang-it').attr('href'))
				}

				$(data.next.container).css('opacity', 0)
				$('.footer').animate({ opacity: 1 }, 250)
				$(data.next.container).animate({ opacity: 1 }, 250, function () {
					done()
				})
			}
		}],
		views: [
			{
				namespace: 'aareufer',
				beforeEnter: function() {
					console.log('aareufer')

					updateLive()

					// CLOCK
					var canvas = $('.clock').get(0)
					var ctx = canvas.getContext('2d')
					var radius = canvas.height / 2
					ctx.translate(radius, radius)
					radius = radius * 0.90

					drawClock()
					clock = setInterval(drawClock, 1000)

					function drawClock() {
						drawFace(ctx, radius)
						drawNumbers(ctx, radius)
						drawTime(ctx, radius)
					}

					function drawFace(ctx, radius) {
						ctx.beginPath()
						ctx.arc(0, 0, radius, 0, 2 * Math.PI)
						ctx.fillStyle = 'black'
						ctx.fill()
					}

					function drawNumbers(ctx, radius) {
						var ang
						var num
						ctx.font = radius * 0.15 + "px arial"
						ctx.textBaseline = "middle"
						ctx.textAlign = "center"
						ctx.fillStyle = '#fff'
						ctx.strokeStyle = '#fff'
						for(num = 1; num < 13; num++){
							ang = num * Math.PI / 6
							ctx.rotate(ang)
							ctx.translate(0, -radius * 0.85)
							ctx.rotate(-ang)
							ctx.fillText(num.toString(), 0, 0)
							ctx.rotate(ang)
							ctx.translate(0, radius * 0.85)
							ctx.rotate(-ang)
						}
					}

					function drawTime(ctx, radius){
						var now = new Date()
						var hour = now.getHours()
						var minute = now.getMinutes()
						var second = now.getSeconds()
						//hour
						hour = hour%12
						hour = (hour*Math.PI/6)+(minute*Math.PI/(6*60))+(second*Math.PI/(360*60))
						drawHand(ctx, hour, radius*0.3, 2)
						//minute
						minute = (minute*Math.PI/30)+(second*Math.PI/(30*60))
						drawHand(ctx, minute, radius*0.6, 2)
						// second
						second = (second*Math.PI/30)
						drawHand(ctx, second, radius*0.8, 1)
					}

					function drawHand(ctx, pos, length, width) {
						ctx.beginPath()
						ctx.lineWidth = width
						ctx.lineCap = "round"
						ctx.moveTo(0,0)
						ctx.rotate(pos)
						ctx.lineTo(0, -length)
						ctx.stroke()
						ctx.rotate(-pos)
					}

					$('.main-live-scroller').marquee({
						duration: 2000,
						duplicated: true,
						startVisible: true
					})

					$('.main-live-block').click(function(){
						barba.go('/'+$(this).data('stream'))
					})
				},
				beforeLeave: function(){
					console.log('leaving aareufer..')

					clearInterval(clock)
				}
			},{
				namespace: 'programm',
				beforeEnter: function() {
					console.log('programm')

					handleFilters()

					$('.main-day-event').click(function(){
						$(this).toggleClass('main-day-flip')
					})

					$('.main-day-register').click(function(e){
						e.stopPropagation()
						$('.registration').fadeIn()
						$('.registration-title').text($(this).data('event-title'))
						$('.registration input[name="event-title"]').val($(this).data('event-title'))
						$('.registration input[name="event-day"]').val($(this).data('event-day'))
						$('.registration input[name="event-room"]').val($(this).data('event-room'))
						$('.registration input[name="event-time"]').val($(this).data('event-time'))
					})
				},
				beforeLeave: function(){
					console.log('leaving program..')
				}
			},{
				namespace: 'logbuch',
				beforeEnter: function() {
					console.log('logbuch')

					handleFilters()

					$('.wp-block-gallery').each(function(){
						var $slides = $(this).find('img')
						$gallery = $(this)
						$gallery.empty()
						$slides.each(function(){
							var $slide = $('<div class="wp-block-gallery-slide">')
							$slide.append($(this))
							console.log($slide)
							$gallery.append($slide)
						})
					})

					$('.wp-block-gallery').flickity({
						// options
						contain: false,
						setGallerySize: false
					})
				},
				beforeLeave: function(){
					console.log('leaving logbuch..')
				}
			},{
				namespace: 'logbuch-map',
				beforeEnter: function() {
					console.log('logbuch map')

					$('.wp-block-gallery').each(function(){
						var $slides = $(this).find('img')
						$gallery = $(this)
						$gallery.empty()
						$slides.each(function(){
							var $slide = $('<div class="wp-block-gallery-slide">')
							$slide.append($(this))
							console.log($slide)
							$gallery.append($slide)
						})
					})

					$('.wp-block-gallery').flickity({
						// options
						contain: false,
						setGallerySize: false
					})

					// handle map
					$('.main-single-logbook-player-play').click(function(){
						$(this).hide()
						$(this).siblings('.main-single-logbook-player-pause').show()
						$(this).siblings('audio').get(0).play()
					})

					$('.main-single-logbook-player-pause').click(function(){
						$(this).hide()
						$(this).siblings('.main-single-logbook-player-play').show()
						$(this).siblings('audio').get(0).pause()
						$(this).siblings('audio').get(0).currentTime = 0
					})

				},
				beforeLeave: function(){
					console.log('leaving logbuch map..')
				}
			},{
				namespace: 'gaeste',
				beforeEnter: function() {
					console.log('gäste')
					handleHash()
				}
			},{
				namespace: 'info',
				beforeEnter: function() {
					console.log('info')
					handleHash()
				}
			},{
				namespace: 'stream',
				beforeEnter: function() {
					console.log('info')
					$('.nav-scroller').marquee('pause')
					refresh = false
					updateLive()
				},
				beforeLeave: function(){
					$('.nav-scroller').marquee('resume')
					refresh = true
					updateLive()
				}
			},{
				namespace: 'bolo',
				beforeEnter: function() {
					console.log('bolo')
					$('.main-bolo-klub-gallery').flickity({
						// options
						contain: false,
						setGallerySize: false,
						imagesLoaded: true
					})

					$('.main-bolo-klub-gallery').on('change.flickity', function(e, index){
						updateMeta(index)
					})

					updateMeta(0)

					function updateMeta(index){
						$slide = $('.main-bolo-klub-slide').eq(index)

						$('.main-bolo-klub-name').attr('href', 'mailto:'+$slide.data('email'))
						$('.main-bolo-klub-name').text($slide.data('name'))

						$('.main-bolo-klub-website').attr('href', $slide.data('website'))
						$('.main-bolo-klub-website').text($slide.data('website'))

						$('.main-bolo-klub-instagram').attr('href', 'https://www.instagram.com/'+$slide.data('instagram').replace('@', ''))
						$('.main-bolo-klub-instagram').text($slide.data('instagram'))

						$('.main-bolo-klub-buy').attr('href', 'mailto:info@literatur.ch?subject=Bolo Klub → '+$slide.data('name'))

						$('.main-bolo-klub-technik').text($slide.data('technik'))
						$('.main-bolo-klub-format').text($slide.data('format'))
						$('.main-bolo-klub-price').text($slide.data('preis'))

						if ($slide.data('sold') == 1) {
							$('.main-bolo-klub-buy').hide()
							$('.main-bolo-klub-sold').show()
						} else {
							$('.main-bolo-klub-buy').show()
							$('.main-bolo-klub-sold').hide()
						}
					}
				}
 			},{
				namespace: 'srf',
				beforeEnter: function() {
					console.log('srf')

					handleFilters()

					$('.main-srf-item').click(function(){
						$(this).toggleClass('main-srf-flip')
					})

					$('.main-srf-link').click(function(e){
						e.stopPropagation()
					})
				},
				beforeLeave: function(){
					console.log('leaving program..')
				}
			}
		]
	})

	// Global UI

	if ($(window).width() < 1024){
		$('.nav').hide()
		mobile = true
	}
	$('.nav').removeClass('no-show')

	$(window).resize(function(){
		if ($(window).width() < 1024){
			$('.nav').hide()
			mobile = true
		} else {
			$('.nav').show()
			mobile = false
		}
	})

	$('.nav-burger').click(function(){
		$('.nav').fadeIn()
	})

	$('.nav-close').click(function(){
		$('.nav').fadeOut()
	})

	$('.registration-close').click(function(){
		$('.registration').fadeOut(function(){
			$('.registration-success').hide()
			$('.registration-failure').hide()
			$('.wpcf7 input[type=submit]').show()
		})
	})

	$('.wpcf7').on( 'wpcf7submit', function() {
		$('.wpcf7 input[type=submit]').hide()
		$('.registration-progress').show()
	})

	$('.wpcf7').on( 'wpcf7mailsent', function() {
		$('.wpcf7 input[type=submit]').hide()
		$('.registration-failure').hide()
		$('.registration-progress').hide()
		$('.registration-success').show()
	})

	$('.wpcf7').on( 'wpcf7invalid', function() {
		$('.registration-failure').show()
		$('.registration-progress').hide()
	})

	// Functions

	function handleHash(){
		console.log('checking location', window.location.hash)
		setTimeout(function(){
			if ( window.location.hash != '') $('html, body').animate({scrollTop: $( window.location.hash).offset().top - 40}, 500)
		}, 250)
	}

	function handleFilters(){
		$('.main-filters-group-item').click(function(){
			$(this).siblings().removeClass('filter-active')
			$(this).toggleClass('filter-active')
			updateFilters()
		})
	}

	function updateFilters(){
		// show/hide items
		$filters = $('.filter-active')
		if ($filters.length) {
			$('.main-item').hide()
			var query = ''
			$('.filter-active').each(function(){
				query += '.main-item-'+$(this).data('key')+'-'+$(this).data('value')
			})
			$(query).show()
		} else $('.main-item').show()

		// show/hide day
		$('.main-day').each(function(){
			if ($(this).find('.main-day-event:visible').length) $(this).find('.main-day-title').show()
			else $(this).find('.main-day-title').hide()
		})
	}

	function updateLive(){
		$.get('/wp-json/slt/live', function(data){
			console.log(JSON.parse(data))
			var live = JSON.parse(data)

			if (refresh) {
				var text = ''
				text += handleScroll(live, 'saulenhalle')
				text += handleScroll(live, 'seminarraum')
				text += handleScroll(live, 'kneipe')
				text += handleScroll(live, 'foyer')

				$('.nav-scroller').marquee('destroy')
				$('.nav-scroller').html(text)
				$('.nav-scroller').marquee({
					duration: 10000,
					duplicated: true,
					startVisible: true
				})
			}

			handleChat(live, 'saulenhalle')
			handleChat(live, 'seminarraum')
			handleChat(live, 'kneipe')
			handleChat(live, 'foyer')

			// handle blocks
			handleBlock(live, 'saulenhalle')
			handleBlock(live, 'seminarraum')
			handleBlock(live, 'kneipe')
			handleBlock(live, 'foyer')

			$('.main-day-event').removeClass('main-day-event-live')
			handleProgram(live.saulenhalle)
			handleProgram(live.seminarraum)
			handleProgram(live.kneipe)
			handleProgram(live.foyer)

		})
	}

	function handleScroll(events, which){
		var t = ''
		if (events[which].status != 'end'){
			if (events[which].status == 'now') t += 'LIVE: '
			else if (events[which].status == 'next') t += events[which].time+': '
			t += '<a href="/'+which+'">→ '+events[which].title+'</a> | '
		}
		return t
	}

	function handleBlock(live, key){
		if (live[key].status == 'end') {
			$('.main-live-'+key).removeClass('main-live-block-on-air')
			$('.main-live-'+key+' .main-live-block-time').empty().hide()
			$('.main-live-'+key+' .main-live-block-title').empty().hide()
			$('.main-live-'+key+' .main-live-scroller').hide()
		} else if (live[key].status == 'next') {
			$('.main-live-'+key).removeClass('main-live-block-on-air')
			$('.main-live-'+key+' .main-live-block-time').text(live[key].time).show()
			$('.main-live-'+key+' .main-live-block-title').text(live[key].title).show()
			$('.main-live-'+key+' .main-live-scroller').hide()
		} else if (live[key].status == 'now') {
			$('.main-live-'+key).addClass('main-live-block-on-air')
			$('.main-live-'+key+' .main-live-block-time').empty().hide()
			$('.main-live-'+key+' .main-live-block-title').text(live[key].title).show()
			$('.main-live-'+key+' .main-live-scroller').show()
			$('.main-live-scroller').marquee('destroy')
			$('.main-live-scroller').marquee({
				duration: 10000,
				duplicated: true,
				startVisible: true
			})
		}
	}

	function handleChat(live, key){
		if (live[key].chat) $('.main-stream-'+key).removeClass('no-chat')
		else $('.main-stream-'+key).addClass('no-chat')
	}

	function handleProgram(data){
		if (data.status == 'now') $('.main-item-id-'+data.id).addClass('main-day-event-live')
	}

	updateLive()

	setInterval(function(){
		updateLive()
	}, 60000)
})