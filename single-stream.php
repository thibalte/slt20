<?php get_header(); ?>
<?php if (have_posts()): while (have_posts()): the_post(); ?>
	<main class="main main-black" data-barba="container" data-barba-namespace="stream" data-room="<?php echo $post->post_name; ?>">
		<div class="main-stream-wrapper main-stream-<?php echo $post->post_name; ?>">
			<div class="main-stream">
				<?php echo get_field('stream'); ?>
			</div>
			<div class="main-chat"<?php if (get_field('stream') == '') { echo ' style="display: block !important; height: 600px;"'; } ?>>
				<?php echo get_field('chat'); ?>
			</div>
		</div>
</main>
<?php endwhile; endif; ?>
<?php get_footer(); ?>