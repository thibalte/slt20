<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<meta name="google" content="notranslate" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta property="og:url" content="https://literatur-online.ch/" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Solothurner Literaturtage 2020" />
	<meta property="og:description" content="Solothurner Literaturtage 2020" />
	<meta property="og:image" content="<?php echo get_stylesheet_directory_uri(); ?>/assets/og@4x.png" />
	<?php wp_head(); ?>
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon.png">
	<title>Solothurner Literaturtage 2020</title>
</head>
<body class="" data-barba="wrapper">
	<!-- NAV !-->
	<nav class="nav no-show">
		<div class="nav-menu">
			<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
		</div>
		<div class="nav-lang">
			<?php
				global $post;
			?>
			<!--
			<a href="<?php echo get_the_permalink(pll_get_post($post->ID, 'de')); ?>" class="no-barba" id="lang-de">DE</a>
			<a href="<?php echo get_the_permalink(pll_get_post($post->ID, 'fr')); ?>" class="no-barba" id="lang-fr">FR</a>
			<a href="<?php echo get_the_permalink(pll_get_post($post->ID, 'it')); ?>" class="no-barba" id="lang-it">IT</a>
			!-->
			<a href="/de/aareufer" data-barba-prevent id="lang-de"<?php if (pll_current_language() == 'de') { echo ' class="lang-active"'; }?>>DE</a>
			<a href="/fr/aareufer-2/" data-barba-prevent id="lang-fr"<?php if (pll_current_language() == 'fr') { echo ' class="lang-active"'; }?>>FR</a>
			<a href="/it/aareufer-3/" data-barba-prevent id="lang-it"<?php if (pll_current_language() == 'it') { echo ' class="lang-active"'; }?>>IT</a>
		</div>
		<div class="nav-info">
			<a href="<?php echo get_the_permalink(pll_get_post(get_page_by_title('Info')->ID)); ?>">i</a>
		</div>
		<div class="nav-close">✕</div>
	</nav>
	<nav class="nav-live" style="display: none;">
		<div class="nav-streams">
			<ul>
				<li><a href="/stream/saulenhalle/">Säulenhalle</a></li>
				<li><a href="/stream/seminarraum/">Seminarraum</a></li>
				<li><a href="/stream/kneipe/">Kneipe</a></li>
				<li><a href="/stream/foyer/">Foyer</a></li>
			</ul>
		</div>
		<div class="nav-scroller"></div>
	</nav>
	<div class="nav-burger">&#9776;</div>