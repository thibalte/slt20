<?php get_header(); ?>
<main class="main main-black" data-barba="container" data-barba-namespace="logbuch">
	<div class="main-filters">
		<div class="main-filters-group main-filters-group-1">
			<div class="main-filters-group-name"><?php str_e('Language'); ?>:</div>
			<div class="main-filters-group-items">
				<div class="main-filters-group-item" data-key="lang" data-value="de">de</div>
				<div class="main-filters-group-item" data-key="lang" data-value="fr">fr</div>
				<div class="main-filters-group-item" data-key="lang" data-value="it">it</div>
				<div class="main-filters-group-item" data-key="lang" data-value="rm">rm</div>
				<div class="main-filters-group-item" data-key="lang" data-value="en">en</div>
			</div>
		</div>
		<div class="main-filters-group main-filters-group-5">
			<div class="main-filters-group-name"><?php str_e('Type'); ?>:</div>
			<div class="main-filters-group-items">
				<div class="main-filters-group-item" data-key="type" data-value="prosa"><?php str_e('Prosa'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="lyrik"><?php str_e('Lyrik'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="spoken"><?php str_e('Spoken Word'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="ubersetzung"><?php str_e('Übersetzung'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="kinder"><?php str_e('Kinder- und Jugendliteratur'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="int-gaste"><?php str_e('Int. Gäste'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="illustration"><?php str_e('Illustration'); ?></div>
				<div class="main-filters-group-item" data-key="type" data-value="dialog"><?php str_e('Im Dialog'); ?></div>
			</div>
		</div>
	</div>
	<div class="main-logbooks">
		<?php
			$logbooks = get_posts( array(
				'posts_per_page'	=> -1,
				'post_type'			=> 'logbook',
				'orderby'			=> 'rand'
			));
		?>
		<?php if ($logbooks): foreach($logbooks as $logbook): ?>
			<div class="main-item main-logbook<?php
				// languages
				$languages = get_field('languages', $logbook->ID);
				if( $languages ):
					foreach( $languages as $language ):
						echo ' main-item-lang-';
						echo $language;
					endforeach;
				endif;
			?> main-item-type-<?php echo get_field('type', $logbook->ID)['value']; ?>">
				<div class="main-logbook-cover">
					<?php
						$cover = false;
						if (get_field('with_cover', $logbook->ID)) $cover = get_field('cover', $logbook->ID);
						if ($cover) {
							echo '<a href="' .get_the_permalink($logbook->ID). '">';
							echo '<img src="' .$cover['sizes']['large']. '">';
							echo '</a>';
						}
					?>
				</div>
				<div class="main-logbook-date">
					<!-- <?php str_e('Logbuch'); ?> <?php echo get_the_date('d.m.Y', $logbook->ID); ?> !-->
				</div>
				<div class="main-logbook-title main-logbook-title-<?php echo get_field('logbuch_type', $logbook->ID); ?>">
					<a href="<?php echo get_the_permalink($logbook->ID); ?>">
						<?php echo get_the_title($logbook->ID); ?>
					</a>
				</div>
				<?php $subtitle = get_field('subtitle', $logbook->ID); ?>
				<?php if ($subtitle): ?>
				<div class="main-logbook-meta">
					<span class="italic"><?php echo $subtitle; ?></span>
				</div>
				<?php endif; ?>

				<!--
				<div class="main-day-inner">
					<div class="main-day-front">
						<div class="main-day-event-time">
							<div class="main-day-event-slot">
								<?php
									echo get_field('time_start', $event->ID);
									if (get_field('with_stop', $event->ID)) {
										echo '&ndash;'. get_field('time_stop', $event->ID);
									}
								?>
							</div>
							<div class="main-day-event-stage">
								<?php echo get_field('stage', $event->ID)['label']; ?>
							</div>
						</div>
						<div class="main-day-event-registration">
							<?php
								$register = get_field('registration', $event->ID);
								if ($register != 'no') {
									str_e('Required');
								}
							?>
						</div>
						<div class="main-day-event-title">
							<?php echo get_the_title($event->ID); ?>
						</div>
						<div class="main-day-event-meta">
							<span class="bold">
								<?php
									// type
									str_e(get_field('type', $event->ID));

									// languages
									$languages = get_field('languages', $event->ID);
									if( $languages ):
										echo '(';
										$first = true;
										foreach( $languages as $language ):
											if (!$first) echo '/';
											echo $language;
											$first = false;
										endforeach;
										echo ')';
									endif;
								?>
							</span>
						</div>
						<div class="main-day-event-more">
							i
							<div class="main-day-event-more-black"></div>
						</div>
					</div>
					<div class="main-day-back">
					</div>
				</div>
				!-->
			</div>
		<?php endforeach; endif; ?>
	</div>
</main>
<?php get_footer(); ?>