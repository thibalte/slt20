<?php
/*
 * Template Name: Logbuch Map
 * Template Post Type: logbook
 */
 ?>

<?php get_header(); ?>
<main class="main" data-barba="container" data-barba-namespace="logbuch-map">
	<div class="main-single-logbook">
		<?php if (have_posts()): while (have_posts()): the_post(); ?>
			<?php if (get_field('with_cover')): ?>
				<?php
					$cover = get_field('cover');
				?>
				<div class="main-single-logbook-hero<?php if (!$cover) { echo ' main-single-logbook-hero-black'; } ?>">
					<?php if ($cover): ?>
					<div class="main-single-logbook-cover">
						<img src="<?php echo $cover['sizes']['large']; ?>">
					</div>
					<?php endif; ?>
					<div class="main-single-logbook-title main-single-logbook-title-<?php echo get_field('logbuch_type'); ?>">
						<?php
							$long = get_field('long_title');
							if ($long == '') echo get_the_title();
							else echo $long;
						?>
					</div>
					<div class="main-single-logbook-subtitle">
						<?php echo get_field('subtitle'); ?>
					</div>
				</div>
			<?php else: ?>
				<div class="main-single-logbook-title-no-cover main-single-logbook-title-<?php echo get_field('logbuch_type'); ?>">
					<?php echo get_field('long_title'); ?>
				</div>
			<?php endif; ?>

			<div class="main-single-logbook-content">
				<?php the_content(); ?>
			</div>

			<div class="main-single-logbook-map">
				<img src="<?php echo get_field('map')['url']; ?>">
				<?php if (have_rows('items')): ?>
				<div class="main-single-logbook-players">
					<?php while(have_rows('items')): the_row(); ?>
						<div class="main-single-logbook-player" style="left: <?php echo get_sub_field('x'); ?>%;top: <?php echo get_sub_field('y'); ?>%;">
							<span class="main-single-logbook-player-play"><?php echo get_sub_field('label'); ?> &#x25b6;</span>
							<span class="main-single-logbook-player-pause" style="display: none;">&#x270b;</span>
							<audio>
								<source src="<?php echo get_sub_field('mp3')['url']; ?>" type="audio/mpeg">
							</audio>
						</div>
					<?php endwhile; ?>
				</div>
				<?php endif; ?>
			</div>

			<?php
				// MODERATOR
				$moderators = get_field('moderator');
				if ($moderators): foreach($moderators as $moderator):
			?>
			<div class="main-single-logbook-moderator">
				<div class="main-single-logbook-moderator-name"><?php echo get_the_title($moderator->ID); ?></div>
				<div class="main-single-logbook-moderator-row">
					<div class="main-single-logbook-moderator-col main-single-logbook-moderator-col-3">
						<p><?php echo get_field('biography', $moderator->ID); ?></p>
					</div>
					<div class="main-single-logbook-moderator-col main-single-logbook-moderator-col-1">
						<img src="<?php echo get_field('portrait', $moderator->ID)['sizes']['large']; ?>">
					</div>
				</div>
			</div>
			<?php endforeach; endif; ?>

			<?php
				$extra_embed = get_field('extra_embed');
				if ($extra_embed != ''):
			?>
			<div class="main-single-logbook-extra-embed">
				<?php echo $extra_embed; ?>
			</div>
			<?php endif; ?>

			<div class="main-single-logbook-writers-books">
				<?php
					// WRITERS
					$writers = get_field('writers');
				?>
				<div class="main-single-logbook-writers">
					<?php if ($writers): foreach($writers as $writer): ?>
					<div class="main-single-logbook-writer">
						<a href="<?php echo get_the_permalink($writer->ID); ?>">
							<?php
								$firstname = get_field('firstname', $writer->ID);
								if ($firstname != '') echo get_field('firstname', $writer->ID) .'<br>';
								echo get_field('lastname', $writer->ID) .'<br>→';
							?>
						</a>
					</div>
					<?php endforeach; endif; ?>
					<?php

						if ($writers):

							$q = array(
								'posts_per_page'	=> -1,
								'post_type'			=> 'event',
								'orderby'			=> 'date',
								'order'				=> 'ASC',
								'meta_query' => array(
									'relation' => 'OR'
								)
							);

							foreach($writers as $writer):

								array_push($q['meta_query'], array(
									'key' => 'writers',
									'value' => '"' . $writer->ID . '"',
									'compare' => 'LIKE'
								));

							endforeach;

							$live = get_posts($q);

						endif;

						if ($live): ?>
						<div class="main-single-logbook-live">
							<h1>LIVE</h1>
							<?php foreach($live as $e): ?>
								<div class="main-single-logbook-event">
									<div class="main-single-logbook-event-col">
										<?php
											str_e(get_field('day', $e->ID));
											echo ' ' .get_field('time_start', $e->ID);
											if (get_field('with_stop', $e->ID)) echo '&ndash;' .get_field('time_stop', $e->ID);
										?>
									</div>
									<div class="main-single-logbook-event-col">
										<?php
											echo get_the_title($e->ID);
										?>
									</div>
									<div class="main-single-logbook-event-col">
										<?php
											echo get_field('stage', $e->ID)['label'];
										?>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
						<?php endif; ?>
				</div>
				<?php
					// BOOKS
					$books = get_field('books');
				?>
				<?php if ($books): ?>
					<div class="main-single-logbook-books">
						<?php  foreach($books as $book): ?>
						<div class="main-single-logbook-book">
							<div class="main-single-logbook-book-cover">
								<img src="<?php echo get_field('cover', $book->ID)['sizes']['large']; ?>">
							</div>
							<div class="main-single-logbook-book-details">
								<h1><?php echo get_the_title($book->ID); ?></h1>
								<p><?php echo get_field('details', $book->ID); ?></p>
								<div class="main-book-buttons">
									<!--
									<span class="main-book-info">
										<a href="<?php echo get_the_permalink($book->ID); ?>"><?php str_e('Info'); ?></a>
									</span>
									!-->
									<?php $buy = get_field('link', $book->ID); ?>
									<?php if ($buy != ''): ?>
									<span class="main-book-buy">
										<a href="<?php echo $buy; ?>" target="_blank"><?php str_e('Buy'); ?></a>
									</span>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach;?>
					</div>
				<?php endif; ?>
				</div>
			</div>
		<?php endwhile; endif; ?>
	</div>
</main>
<?php get_footer(); ?>