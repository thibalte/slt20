<?php /* Template Name: Wine */ ?>

<?php get_header(); ?>
<main class="main main-black" data-barba="container">
	<div class="main-resonanzraum">
		<?php if (have_posts()): while (have_posts()): the_post(); ?>
			<div class="main-resonanzraum-content">
				<h1><?php echo get_the_title(); ?></h1>
				<div class="main-resonanzraum-content-buffer">
					<?php the_content(); ?>
				</div>
			</div>
		<?php endwhile; endif; ?>
	</div>
</main>
<?php get_footer(); ?>